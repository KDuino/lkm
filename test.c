#include <stdio.h>
#include <fcntl.h>
#include "version.h"

int main(void) {
  int fd = open("/dev/version", O_RDWR);
  if (ioctl(fd, 0)) {
    printf("Version has been modified, reset it now!\n");
    ioctl(fd, 1);
  } else {
    printf("Version has not been modified.\n");
  }
  return 0;
}
