#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <uapi/asm-generic/errno-base.h>
#include <uapi/asm-generic/errno.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/utsname.h>
#include <linux/slab.h>
#include "version.h"

static char *base_version = NULL;
static char *current_version = NULL;

static inline char *strlconcat(char *str1,
			       const char *str2)
{
  size_t len = strlen(str1) + strlen(str2) + 1;
  char *dst = kmalloc(len, GFP_KERNEL);

  if (dst != NULL) {
    strlcpy(dst, str1, len);
    strlcat(dst, str2, len);
  }
  return dst;
}

static inline int init_current_version_string(void)
{
  if (current_version != NULL) {
    kfree(current_version);
  }
  //current_version = strlconcat(base_version, "\n");
  current_version = kstrdup(base_version, GFP_KERNEL);
  return (current_version == NULL) ? -1 : 0;
}

static long version_modified(void)
{
  return strcmp(base_version, current_version);
}

static long version_reset(void)
{
  return init_current_version_string();
}

typedef long (*cmd_table)(void);

static long version_ioctl(struct file *file,
			  unsigned int cmd,
			  unsigned long arg)
{
  static cmd_table table[2] = {
    version_modified,
    version_reset
  };
  if (cmd >= 2) {
    return -1;
  } else {
    return table[cmd]();
  }
}

static ssize_t version_write(struct file *file,
			     const char __user *ubuf,
			     size_t count,
			     loff_t *ppos)
{
  char *dest;
  char *dest2;
  size_t len = count + 1;
  size_t copied = count;

  if (!strcmp(base_version, current_version)) {
    if (current_version != NULL) {
      kfree(current_version);
    }
    current_version = NULL;
  } else {
    len += strlen(current_version);
  }
  dest = kmalloc(len, GFP_KERNEL);
  if (dest == NULL) {
    return -1;
  }
  dest2 = dest;
  if (current_version != NULL) {
    strlcpy(dest, current_version, len);
    while (*dest) {
      dest++;
      len--;
    }
  }
  current_version = dest2;
  while (count--) {
    get_user(*dest++, ubuf++);
  }
  *dest = '\0';
  return copied;
}

static ssize_t version_read(struct file *file,
			    char __user *ubuf,
			    size_t count,
			    loff_t *ppos)
{
  static char *version_ptr = NULL;
  ssize_t length = 0;

  if (version_ptr == NULL) {
    version_ptr = current_version;
  } else if (!*version_ptr) {
    version_ptr = current_version;
    return 0;
  }
  while (count-- && *version_ptr) {
    put_user(*version_ptr++, ubuf++);
    length++;
  }
  if (!*version_ptr) {
    put_user('\n', ubuf++);
    length++;
  }
  return length;
}

static struct file_operations operations = {
  .owner = THIS_MODULE,
  .read = version_read,
  .write = version_write,
  .unlocked_ioctl = version_ioctl,
};

static struct miscdevice device = {
  .minor = MISC_DYNAMIC_MINOR,
  .name = "version",
  .fops = &operations,
};

static int __init version_init(void)
{
  base_version = utsname()->version;
  if (init_current_version_string() < 0) {
    return -1;
  }
  return misc_register(&device);
}

static void __exit version_exit(void)
{
  kfree(current_version);
  misc_deregister(&device);
}

module_init(version_init);
module_exit(version_exit);
MODULE_AUTHOR("Guillaume Pirou <guillaume.pirou@epitech.eu>");
MODULE_AUTHOR("Lucas Languedoc <lucas.languedoc@epitech.eu>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Miscellaneaous device returning and changing the current "
		   "version of the kernel");
